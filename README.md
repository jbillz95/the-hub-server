#Social Media Hub Backend Server
##Setup Instructions
1. Clone the repo
2. Run `npm install` to install required libraries/dependencies
3. Navigate to install directory and create folder `mkdir security`
4. Follow [INSTRUCTIONS](https://www.akadia.com/services/ssh_test_certificate.html) (Steps 1-4) to generate certificates needed to use HTTPS
5. Boot up server by running `node ./src/app.js` from root install directory
