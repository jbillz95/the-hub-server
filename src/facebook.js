const FB = require('fb');
const _ = require('lodash');

const fbConfig = {};

const setAccessToken = (token) => {
  fbConfig.setAccessToken = token;
  FB.setAccessToken(token);

  const fields = { fields: ['id', 'name'] };
  FB.api('me', fields, (results) => {
    fbConfig.profile = results;
  });

  return { success: true };
};

const getMyProfile = (params) => {
  return new Promise((resolve, reject) => {
    FB.api('me', params, (results) => {
      fbConfig.profile = results;
      resolve(results);
    });
  });
};

const getMyFeed = (params) => {
  return new Promise((resolve) => {
    FB.api('me/feed', params, (results) => {
      resolve(results);
    });
  });
};

const getPostAttributes = (params) => {
  const { fields, postId } = params;

  return new Promise((resolve) => {
    FB.api('me/feed', { fields }, (results) => {
      resolve(_.find(results.data, (data) => data.id === postId));
    });
  });
};

module.exports = {
  fbConfig,
  getMyFeed,
  getMyProfile,
  getPostAttributes,
  setAccessToken
};
