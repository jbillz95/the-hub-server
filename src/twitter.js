const _ = require('lodash');
const Twit = require('twit');

const fs = require('fs');

const config = {
  consumer_key: '1bgAuk8morUlsnFsPaqP1Kyvg',
  consumer_secret: 'bXVulLGtcHjWgcDWxrHGgqmbSyHm0nMdWkvigVpEuWcMRrkkhG'
};

let twitterApi;

//TODO: Currently, if the user doesn't login first, then getLatestTweet crashes the server...
const loginUser = (_accessToken, _accessTokenSecret) => {
  config.access_token = _accessToken;
  config.access_token_secret = _accessTokenSecret;

  twitterApi = new Twit(config);
}

const getMyLatestTweets = (params) => {
  return twitterApi.get('statuses/user_timeline', params).then((results) => {
    return results.data;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

const postTweet = (params) => {
  return twitterApi.post('statuses/update', params).then((result) => {
    //Result is either a tweet object or an array of errors
    return result.data;
  }).catch((error) => {
    console.log(`Error posting tweet: ${error}`);
  });
}

const deleteTweet = (params) => {
  return twitterApi.post('statuses/destroy/:id', params).then((result) => {
    return result.data;
  }).catch((error) => {
    console.log(`Error deleting tweet: ${error}`);
  })
}

const postTweetImg = (dataImageUrl) => {
  const statusJson = JSON.stringify(dataImageUrl);
  const base64Url = statusJson.split(',')[1];

  const status = JSON.stringify(statusJson.split(',')[2]);
  const statusText = status.substring(3, status.length - 5);

  let media_id_string;
  return twitterApi.post('media/upload', { media_data: base64Url }).then((result) => {
    media_id_string = result.data.media_id_string;
    const meta_params = {media_id: media_id_string, alt_text: { text: 'example'}};
    return twitterApi.post('media/metadata/create', meta_params);
  }).then((results) => {
    return results;
  }).then((statusResults) => {
    const params = { status: statusText, media_ids: [media_id_string] }

    return twitterApi.post('statuses/update', params );
  }).catch((error) => {
    console.log(`Error on tweet image posting: ${error}`);
  });
}

const verifyCredentials = () => {
  if (!twitterApi) {
    console.log('Twitter API NOT initialized... User has likely not logged in.');
    return new Promise((resolve) => {
      resolve(null);
    });
  }

  return twitterApi.get('account/verify_credentials', { skip_status: true }).then((result) => {
    return result.data;
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
}

module.exports = {
  deleteTweet,
  getMyLatestTweets,
  loginUser,
  postTweet,
  postTweetImg,
  verifyCredentials
};
