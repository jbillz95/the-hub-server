const express = require('express');
const cors = require('cors');
const app = express();
const url = require('url');
const fs = require('fs');
const https = require('https');
const _ = require('lodash');

const { deleteTweet, getMyLatestTweets, postTweet, verifyCredentials, postTweetImg } = require('./twitter.js');
const {
  fbConfig,
  getMyFeed,
  getMyProfile,
  getPostAttributes,
  setAccessToken
} = require('./facebook.js');

const securityOptions = {
  key: fs.readFileSync('security/server.key'),
  cert: fs.readFileSync('security/server.crt'),
  requestCert: false,
  rejectUnauthorized: false
};

app.use(cors({
  origin: true,
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
  credentials: true,
  exposedHeaders: ['x-auth-token']
}));
app.disable('etag');

//TODO: It might be cleaner to use: https://medium.com/trisfera/using-cors-in-express-cac7e29b005b
app.use((req, res, next) => {
  res.set('Access-Control-Allow-Origin', `${req.headers.origin}`);
  res.set('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.set('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const bodyParser = require('body-parser');
const request = require('request');
const passport = require('passport');
const TwitterTokenStrategy = require('passport-twitter-token');
const passportConfig = require('./twitter-passport-config.js');
const twitterConfig = {
  consumerKey: '1bgAuk8morUlsnFsPaqP1Kyvg',
  consumerSecret: 'bXVulLGtcHjWgcDWxrHGgqmbSyHm0nMdWkvigVpEuWcMRrkkhG'
};
passportConfig();

app.use(bodyParser.urlencoded({
  extended: true,
  limit: '50mb'
}));
app.use(bodyParser.json({
  limit: '50mb'
}));

let user;

const createToken = (auth) => {
  return jwt.sign({
    id: auth.id
  }, 'my-secret',
  {
    expiresIn: 60*120
  });
};

const generateToken = (req, res, next) => {
  req.token = createToken(req.auth);
  return next();
};

const sendToken = (req, res) => {
  res.setHeader('x-auth-token', req.token);
  return res.status(200).send(JSON.stringify(req.user));
}

//TODO: Do we really need this?
const authenticate = expressJwt({
  secret: 'my-secret',
  requestProperty: 'auth',
  getToken: function(req) {
    if (req.headers['x-auth-token']) {
      return req.headers['x-auth-token'];
    }
    return null;
  }
});

app.post('/api/login', (req, res, next) => {
  request.post({
    url: `https://api.twitter.com/oauth/access_token?oauth_verifier`,
    oauth: {
      consumer_key: 'KEY',
      consumer_secret: 'SECRET',
      token: req.query.oauth_token
    },
    form: { oauth_verifier: req.query.oauth_verifier }
  }, function (err, r, body) {
    if (err) {
      return res.send(500, { message: err.message });
    }

    const bodyString = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
    const parsedBody = JSON.parse(bodyString);

    req.body['oauth_token'] = parsedBody.oauth_token;
    req.body['oauth_token_secret'] = parsedBody.oauth_token_secret;
    req.body['user_id'] = parsedBody.user_id;

    next();
  });
}, passport.authenticate('twitter-token', {session: false}), function(req, res, next) {
    if (!req.user) {
      return res.send(401, 'User Not Authenticated');
    }

    // prepare token for API
    req.auth = {
      id: req.user.id
    };

    verifyCredentials().then((_user) => {
      user = _user;
    }).catch((error) => {
      console.log(`ERROR: ${error}`);
    });

    return next();
}, generateToken, sendToken);

app.post('/api/login/reverse', (req, res) => {
  request.post({
    url: 'https://api.twitter.com/oauth/request_token',
    oauth: {
        oauth_callback: "http%3A%2F%2Flocalhost%3A5000%2Ftwitter-callback",
        consumer_key: twitterConfig.consumerKey,
        consumer_secret: twitterConfig.consumerSecret
      }
  }, (err, r, body) => {
    if (err) {
      return res.send(500, { message: err.message });
    }

    const jsonStr = '{ "' + body.replace(/&/g, '", "').replace(/=/g, '": "') + '"}';
    res.send(JSON.parse(jsonStr));
  });
});


app.get('/', (req, res) => {
  return res.send({ 'mesage': 'Hello World' });
});

app.get('/latestTweet', (req, res) => {
  getLatestTweet(req, res);
});

app.get('/myLatestTweets', (req, res) => {
  const urlParts = url.parse(req.url, true);

  const options = urlParts.query;

  getMyLatestTweets(options).then((tweets) => {
    res.send(JSON.stringify(tweets));
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
});

app.post('/tweet', (req, res) => {
  const body = req.body;

  postTweet(body).then((newTweet) => {
    //Send back the tweet or error.
    res.send(JSON.stringify(newTweet));
  }).catch((error) => {
    console.log(error);
  });
});

app.get('/currentUser', (req, res) => {
  verifyCredentials().then((user) => {
    res.send(JSON.stringify(user));
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  });
});

app.post('/tweetImg', (req, res) => {
  const body = req.body;

  postTweetImg(body).then((img) => {
    res.send(img);
  }).catch((error) =>{
    console.log(error);
  });
});

app.post('/tweet/delete', (req, res) => {
  const body = req.body;

  deleteTweet(body).then((result) => {
    res.send(result);
  }).catch((error) => {
    console.log(`ERROR: ${error}`);
  })
});

/** FACEBOOK ROUTES **/

//Accept access token from client and set to state
app.post('/facebook/accessToken', (req, res) => {
  const response = setAccessToken(req.body.accessToken);
  console.log(`Access Token: ${req.body.accessToken} has been set`);

  res.send(response);
});

//Hit FB API for current user's feed, send back to client
app.get('/facebook/myWholeFeed', (req, res) => {
  const fields = ['created_time', 'id', 'name', 'message', 'full_picture', 'shares'];

  getMyFeed({ fields }).then((feed) => {
    res.send(feed.data);
  }).catch((error) => {
    console.log(error);
  })
});

app.get('/facebook/myTextPosts', (req, res) => {
  const fields = ['created_time', 'from', 'id', 'name', 'message', 'picture', 'shares'];

  getMyFeed({ fields }).then((feed) => {
    if (feed.error) {
      throw feed;
    }

    //Filter posts that have pictures
    return _.filter(feed.data, (post) => !post.picture);
  }).then((textPosts) => {
    //Filter out posts that don't have a message (status)
    return _.filter(textPosts, (post) => post.message);
  }).then((textPosts) => {
    //Only return posts originating from this user
    return _.filter(textPosts, (post) => post.from.id === fbConfig.profile.id);
  }).then((filteredPosts) => {
    res.send(filteredPosts);
  }).catch((error) => {
    console.log(error);
    res.send(error);
  });
});

app.get('/facebook/myPicturePosts', (req, res) => {
  const fields = ['created_time', 'from', 'id', 'name', 'message', 'full_picture', 'shares'];

  getMyFeed({ fields }).then((feed) => {
    if (feed.error) {
      throw feed;
    }

    return _.filter(feed.data, (post) => post.full_picture);
  }).then((textPosts) => {
    return _.filter(textPosts, (post) => post.from.id === fbConfig.profile.id);
  }).then((filteredPosts) => {
    res.send(filteredPosts);
  }).catch((error) => {
    console.log(error);
    res.send(error);
  });
});

//Hit FB API for current user's profile, send back to client.
app.get('/facebook/me', (req, res) => {
  const fields = ['id', 'name', 'birthday', 'email'];

  getMyProfile({ fields }).then((fbProfile) => {
    res.send(fbProfile);
  }).catch((error) => {
    console.log(error);
  });
});

app.get('/facebook/post/metrics/:id', (req, res) => {
  const fields = ['id', 'shares', 'likes.summary(true).limit(0)', 'comments.summary(true).limit(0)'];
  const params = {
    fields,
    postId: req.params.id
  };

  getPostAttributes(params).then((fullPost) => {
    const metrics = {
      id: fullPost.id,
      commentCount: _.get(fullPost, 'comments.summary.total_count') || 0,
      likeCount: _.get(fullPost, 'likes.summary.total_count') || 0
    }

    return metrics;
  }).then((postMetrics) => {
    res.status(200).send(postMetrics);
  });
});

const server = https.createServer(securityOptions, app).listen(5000, () => console.log('Server listening at 5000'));
