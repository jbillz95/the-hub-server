const passport = require('passport');
const TwitterTokenStrategy = require('passport-twitter-token');
const { loginUser } = require('./twitter.js');

module.exports = () => {
  passport.use(
    new TwitterTokenStrategy({
      consumerKey: '1bgAuk8morUlsnFsPaqP1Kyvg',
      consumerSecret: 'bXVulLGtcHjWgcDWxrHGgqmbSyHm0nMdWkvigVpEuWcMRrkkhG',
      includeEmail: true
    }, (token, tokenSecret, profile, done) => {
      //TODO: upsert user to firebase and then return result
      loginUser(token, tokenSecret);
      done(null, {
        token,
        tokenSecret,
        id: profile.id
      });
    }));
}
